package com.zdy.yeb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zdy.yeb.pojo.Department;
import com.zdy.yeb.pojo.RespBean;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
public interface IDepartmentService extends IService<Department> {

    /**
     * 获取所有部门
     *
     * @return
     */
    List<Department> getAllDepartments();

    /**
     * 添加部门
     *
     * @param department
     * @return
     */
    RespBean addDepart(Department department);

    /**
     * 删除部门
     *
     * @param id
     * @return
     */
    RespBean deleteDepart(Integer id);
}
