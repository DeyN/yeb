package com.zdy.yeb.service;

import com.zdy.yeb.pojo.EmployeeRemove;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
public interface IEmployeeRemoveService extends IService<EmployeeRemove> {

}
