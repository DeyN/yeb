package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.EmployeeEc;
import com.zdy.yeb.mapper.EmployeeEcMapper;
import com.zdy.yeb.service.IEmployeeEcService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class EmployeeEcServiceImpl extends ServiceImpl<EmployeeEcMapper, EmployeeEc> implements IEmployeeEcService {

}
