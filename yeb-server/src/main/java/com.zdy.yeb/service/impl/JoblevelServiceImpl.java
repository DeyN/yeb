package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.Joblevel;
import com.zdy.yeb.mapper.JoblevelMapper;
import com.zdy.yeb.service.IJoblevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class JoblevelServiceImpl extends ServiceImpl<JoblevelMapper, Joblevel> implements IJoblevelService {

}
