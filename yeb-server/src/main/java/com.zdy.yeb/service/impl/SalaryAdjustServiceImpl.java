package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.SalaryAdjust;
import com.zdy.yeb.mapper.SalaryAdjustMapper;
import com.zdy.yeb.service.ISalaryAdjustService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class SalaryAdjustServiceImpl extends ServiceImpl<SalaryAdjustMapper, SalaryAdjust> implements ISalaryAdjustService {

}
