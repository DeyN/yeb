package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.SysMsg;
import com.zdy.yeb.mapper.SysMsgMapper;
import com.zdy.yeb.service.ISysMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class SysMsgServiceImpl extends ServiceImpl<SysMsgMapper, SysMsg> implements ISysMsgService {

}
