package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.AdminRole;
import com.zdy.yeb.mapper.AdminRoleMapper;
import com.zdy.yeb.service.IAdminRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole> implements IAdminRoleService {

}
