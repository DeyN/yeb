package com.zdy.yeb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zdy.yeb.mapper.DepartmentMapper;
import com.zdy.yeb.pojo.Department;
import com.zdy.yeb.pojo.RespBean;
import com.zdy.yeb.service.IDepartmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

    @Resource
    private DepartmentMapper departmentMapper;

    /**
     * 获取所有部门
     *
     * @return
     */
    @Override
    public List<Department> getAllDepartments() {
        return departmentMapper.getAllDepartments(-1);
    }

    /**
     * 添加部门
     *
     * @param department
     * @return
     */
    @Override
    public RespBean addDepart(Department department) {
        department.setEnabled(true);
        departmentMapper.addDepart(department);
        if (1 == department.getResult()) {
            return RespBean.success("添加成功！", department);
        }
        return RespBean.error("添加失败！");
    }

    /**
     * 删除部门
     *
     * @param id
     * @return
     */
    @Override
    public RespBean deleteDepart(Integer id) {
        Department department = new Department();
        department.setId(id);
        departmentMapper.deleteDepart(department);
        if (department.getResult() == -2) {
            return RespBean.error("该部门下还有子部门，删除失败！");
        }
        if (department.getResult() == -1) {
            return RespBean.error("该部门下还有员工，删除失败！");
        }
        if (department.getResult() == 1) {
            return RespBean.success("删除成功！");
        }
        return RespBean.error("删除失败！");
    }
}
