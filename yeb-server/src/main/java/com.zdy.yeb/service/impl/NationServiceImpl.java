package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.Nation;
import com.zdy.yeb.mapper.NationMapper;
import com.zdy.yeb.service.INationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class NationServiceImpl extends ServiceImpl<NationMapper, Nation> implements INationService {

}
