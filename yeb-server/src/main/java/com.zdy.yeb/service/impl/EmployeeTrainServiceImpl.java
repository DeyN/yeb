package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.EmployeeTrain;
import com.zdy.yeb.mapper.EmployeeTrainMapper;
import com.zdy.yeb.service.IEmployeeTrainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class EmployeeTrainServiceImpl extends ServiceImpl<EmployeeTrainMapper, EmployeeTrain> implements IEmployeeTrainService {

}
