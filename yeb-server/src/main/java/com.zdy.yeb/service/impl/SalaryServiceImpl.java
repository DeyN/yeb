package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.Salary;
import com.zdy.yeb.mapper.SalaryMapper;
import com.zdy.yeb.service.ISalaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class SalaryServiceImpl extends ServiceImpl<SalaryMapper, Salary> implements ISalaryService {

}
