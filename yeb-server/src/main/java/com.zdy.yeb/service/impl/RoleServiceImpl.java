package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.Role;
import com.zdy.yeb.mapper.RoleMapper;
import com.zdy.yeb.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
