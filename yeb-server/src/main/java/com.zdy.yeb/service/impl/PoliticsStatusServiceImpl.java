package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.PoliticsStatus;
import com.zdy.yeb.mapper.PoliticsStatusMapper;
import com.zdy.yeb.service.IPoliticsStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class PoliticsStatusServiceImpl extends ServiceImpl<PoliticsStatusMapper, PoliticsStatus> implements IPoliticsStatusService {

}
