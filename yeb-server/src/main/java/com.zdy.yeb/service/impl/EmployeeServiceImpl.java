package com.zdy.yeb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zdy.yeb.pojo.Employee;
import com.zdy.yeb.mapper.EmployeeMapper;
import com.zdy.yeb.pojo.RespBean;
import com.zdy.yeb.service.IEmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  员工管理服务类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

    @Override
    public List<Employee> getAllEmployees() {
        return baseMapper.selectList(new QueryWrapper<>());
    }

    @Override
    public RespBean deleteEmployee(Integer id) {
        int num = baseMapper.deleteById(id);
        if (num < 0) {
            return RespBean.error("删除失败");
        }
        return RespBean.success("删除成功");
    }

    @Override
    public RespBean getEmployeeById(Integer id) {
        Employee employee = baseMapper.selectById(id);
        if (Objects.isNull(employee)) {
            return RespBean.error("未查询到对应的员工信息，请核对！");
        }
        return RespBean.success(employee);
    }

    @Override
    public RespBean saveEmployee(Employee employee) {
        int result = baseMapper.insert(employee);
        if (1 == result) {
            return RespBean.success("添加成功！");
        }
        return RespBean.error("添加失败！");
    }
}
