package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.EmployeeRemove;
import com.zdy.yeb.mapper.EmployeeRemoveMapper;
import com.zdy.yeb.service.IEmployeeRemoveService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class EmployeeRemoveServiceImpl extends ServiceImpl<EmployeeRemoveMapper, EmployeeRemove> implements IEmployeeRemoveService {

}
