package com.zdy.yeb.service.impl;

import com.zdy.yeb.pojo.MailLog;
import com.zdy.yeb.mapper.MailLogMapper;
import com.zdy.yeb.service.IMailLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@Service
public class MailLogServiceImpl extends ServiceImpl<MailLogMapper, MailLog> implements IMailLogService {

}
