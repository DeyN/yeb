package com.zdy.yeb.service;

import com.zdy.yeb.pojo.Employee;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zdy.yeb.pojo.RespBean;

import java.util.List;

/**
 * <p>
 *  员工管理服务类
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
public interface IEmployeeService extends IService<Employee> {

    /**
     * 获取所有的员工数据
     * @return
     */
    List<Employee> getAllEmployees();

    /**
     * 根据ID删除员工信息
     * @param id
     * @return
     */
    RespBean deleteEmployee(Integer id);

    /**
     * 根据ID获取员工信息
     * @param id
     * @return
     */
    RespBean getEmployeeById(Integer id);

    RespBean saveEmployee(Employee employee);
}
