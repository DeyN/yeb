package com.zdy.yeb.utils;

import com.zdy.yeb.pojo.Admin;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 操作员工具栏
 * @author zhaody
 * @create 2021-05-09-11:59
 **/
public class AdminUtils {

    public static Admin getCurrentAdmin(){
        return (Admin) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
