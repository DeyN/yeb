package com.zdy.yeb.mapper;

import com.zdy.yeb.pojo.AdminRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zdy.yeb.pojo.RespBean;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
public interface AdminRoleMapper extends BaseMapper<AdminRole> {

    /**
     * 更新操作员角色
     * @param adminId
     * @param rids
     * @return
     */
    Integer addAdminRole(@Param("adminId") Integer adminId, @Param("rids") Integer[] rids);
}
