package com.zdy.yeb.mapper;

import com.zdy.yeb.pojo.SysMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
public interface SysMsgMapper extends BaseMapper<SysMsg> {

}
