package com.zdy.yeb.mapper;

import com.zdy.yeb.pojo.Appraise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
public interface AppraiseMapper extends BaseMapper<Appraise> {

}
