package com.zdy.yeb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zdy.yeb.pojo.Department;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
public interface DepartmentMapper extends BaseMapper<Department> {

    List<Department> getAllDepartments(Integer parentId);

    void addDepart(Department department);

    void deleteDepart(Department department);
}
