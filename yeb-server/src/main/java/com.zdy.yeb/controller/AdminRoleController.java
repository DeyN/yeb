package com.zdy.yeb.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@RestController
@RequestMapping("/admin-role")
@Api(tags = {"角色管理"})
public class AdminRoleController {

}
