package com.zdy.yeb.controller;


import com.zdy.yeb.pojo.Employee;
import com.zdy.yeb.pojo.RespBean;
import com.zdy.yeb.service.IEmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  员工管理控制器
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@RestController
@RequestMapping("/employee")
@Api(tags = {"员工管理"})
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    @ApiOperation("获取所有员工信息")
    @GetMapping("/all")
    public RespBean getAllEmployees() {
        List<Employee> employees = employeeService.getAllEmployees();
        return RespBean.success(employees);
    }

    @ApiOperation("新增员工")
    @PostMapping("/add")
    public RespBean saveEmployee(@RequestBody Employee employee) {
        return employeeService.saveEmployee(employee);
    }

    @ApiOperation("根据id删除员工信息")
    @DeleteMapping("/delete")
    public RespBean deleteEmployee(Integer id) {
        return employeeService.deleteEmployee(id);
    }

    @ApiOperation("更新员工信息")
    @PutMapping("update")
    public RespBean updateEmployee(@RequestBody Employee employee) {
        if (employeeService.updateById(employee)){
            return RespBean.success("更新成功");
        }
        return RespBean.error("更新失败");
    }

    @ApiOperation("根据id获取员工数据")
    @GetMapping("/get")
    public RespBean getEmployeeById(Integer id) {
        return employeeService.getEmployeeById(id);
    }

}
