package com.zdy.yeb.controller;


import com.zdy.yeb.pojo.Menu;
import com.zdy.yeb.service.IAdminService;
import com.zdy.yeb.service.IMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@RestController
@RequestMapping("/system/cfg")
@Api(tags = {"菜单管理"})
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @ApiOperation("通过用户id查询菜单列表")
    @GetMapping("/menu")
    public List<Menu> getMenusByAdminId() {
        return menuService.getMenusByAdminId();
    }
}
