package com.zdy.yeb.controller;


import com.zdy.yeb.pojo.Department;
import com.zdy.yeb.pojo.RespBean;
import com.zdy.yeb.service.IDepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 部门管理 控制器
 * </p>
 *
 * @author zhaody
 * @since 2021-04-19
 */
@RestController
@RequestMapping("/system/basic/department")
@Api(tags = {"部门管理"})
public class DepartmentController {


    @Autowired
    private IDepartmentService departmentService;

    /**
     * 获取所有部门
     *
     * @return
     */
    @ApiOperation("获取所有部门")
    @GetMapping("/")
    public List<Department> getAllDepartments() {
        return departmentService.getAllDepartments();
    }

    @ApiOperation("添加部门")
    @PostMapping("/")
    public RespBean addDepart(@RequestBody Department department) {
        return departmentService.addDepart(department);
    }

    @ApiOperation("删除部门")
    @DeleteMapping("/{id}")
    public RespBean deleteDepart(@PathVariable("id") Integer id) {
        return departmentService.deleteDepart(id);
    }

}
